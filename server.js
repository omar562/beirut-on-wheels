const express = require('express');
const bodyParser = require('body-parser')

const app = express();
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({
    extended: true
}))

const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectId

var db

MongoClient.connect('mongodb://user1:user1@ds159845.mlab.com:59845/bow', (err, database) => {
    if (err) return console.log(err)
    db = database
    app.listen(3000, function () {
        console.log('listening on 3000')
    })
})

var collectionOne = []; 
var collectionTwo = []; 
var collectionThree = []; 

app.get('/', (req, res) => {
   db.collection('vehicles').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionOne[i] = results[i]; 
        }
    })
    db.collection('equipments').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionTwo[i] = results[i]; 
        }
    })
    db.collection('addons').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionThree[i] = results[i]; 
        }
    })
    res.render('index.ejs', {
        vehicles: collectionOne,
        equipments: collectionTwo,
        addons: collectionThree
    })
})

app.get('/vehicles', (req, res) => {
    db.collection('vehicles').find().toArray(function (err, results) {
        if (err) return console.log(err)
        res.render('addvehicle.ejs', {
            vehicles: results
        })

    })
})
app.post('/addv', (req, res) => {
  db.collection('vehicles').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('saved to database')
    res.redirect('/vehicles')
  })
})


app.get('/equipments', (req, res) => {
    db.collection('equipments').find().toArray(function (err, results) {
        if (err) return console.log(err)
        res.render('addequipment.ejs', {
            equipments: results
        })
    })
})
app.post('/adde', (req, res) => {
  db.collection('equipments').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('saved to database')
    res.redirect('/equipments')
  })
})

app.get('/addons', (req, res) => {
    db.collection('addons').find().toArray(function (err, results) {
        if (err) return console.log(err)
        res.render('addons.ejs', {
            addons: results
        })

    })
})
app.post('/addo', (req, res) => {
  db.collection('addons').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('saved to database')
    res.redirect('/addons')
  })
})

app.all('/contact', (req, res) =>{
    res.render('contact.ejs', {
        })
})
app.all('/login', (req, res) =>{
    res.render('login.ejs', {
        })
})
app.all('/signup', (req, res) =>{
    res.render('signup.ejs', {
        })
})

app.all('/employees', (req, res) => {
    res.render('employees.ejs', {
        })
})

app.all('/loggingin/:username', (req, res, next) =>{
    const {
        username
    } = req.params
    
    console.log("username" + username)

    // decoding the id
    
        
    db.collection('vehicles').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionOne[i] = results[i]; 
        }
    })
    db.collection('equipments').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionTwo[i] = results[i]; 
        }
    })
    db.collection('addons').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            collectionThree[i] = results[i]; 
        }
    })
    db.collection('names')
        .findOne({
            username
        }, function (err, result) {
            if (err) {
                return next(err);
            }
            console.log(result)
            // displaying a page where update is made
            // this page contains 2 inputs fields (one for title, one for text) 
        res.render('loggedin.ejs', {
            vehicles: collectionOne,
            equipments: collectionTwo,
            addons: collectionThree,
            results: result
        })
    })
})

app.all('/signingup', (req, res, next) =>{
    console.log(req.body.username)
    db.collection('names').save(req.body, (err, result) => {
    if (err) return console.log(err)
    console.log('saved to database')
    res.redirect('/loggingin/'+req.body.username)
  })
})

app.all('/signingin', (req, res, next) =>{
    
    if (req.body.username == 'Nader Kahwaji' && req.body.password == 'testing'){
        res.redirect('/employees')   
    }
    else {
    console.log(req.body)
    var pass = req.body.password
    var username = req.body.username
     db.collection('names')
        .findOne({
            username
        }, function (err, result) {
            if (err) {
                return next(err);
            }
         console.log(result)
         if (result.password == pass)
             res.redirect('/loggingin/'+req.body.username)
        else 
            res.redirect('/login')
     })
    }
})

app.all('/logout', (req, res) => {
    res.redirect('/')
}) 

var CollectionFour = [];

app.all('/editv/:id', (req, res, next) => {
    
    db.collection('vehicles').find().toArray(function (err, results) {
        if (err) return console.log(err)
        for (var i =0;i<results.length;i++){
            CollectionFour[i] = results[i]; 
        }
    })
    
    const {
        id
    } = req.params

    // decoding the id
    const _id = ObjectId(decodeURI(id))

    db.collection('vehicles')
        .findOne({
            _id
        }, function (err, result) {
            if (err) {
                return next(err);
            }
            console.log(result)

        res.render('update.ejs', { result: result, vehicles: CollectionFour })
    })
})

app.all('/updatev/:id', (req, res, next) =>{
     const {
            id
        } = req.params
     
        const _id = ObjectId(decodeURI(id))
     
        db.collection('vehicles')
            .findOneAndUpdate({
                    _id: _id
                }, 
                    {
                    $set: {
                        brand: req.body.brand,
                        model: req.body.model,
                        type: req.body.type,
                        price: req.body.price,
                        color: req.body.color,
                        sale: req.body.sale,
                        transmission: req.body.transmission
                    }
                },
                function (err, result) {
                    if (err) {
                        return next(err)
                    }
                console.log(result)
                    res.redirect('/vehicles')
                })
})

app.all('/deletev/:id', (req, res, next) => {
    // getting the id of the post chosen
    const {
        id
    } = req.params

    //ObjectID takes an id and returns it's instance
    const _id = ObjectId(decodeURI(id))

    // removing the element that has this id from database
    db.collection('vehicles')
        .remove({
            _id
        }, function (err, result) {
            if (err) {
                return next(err);
            }
            // redirecting the user to main page
            res.redirect('/vehicles')
        })
})

app.all('/rental/:username/:id', (req, res, next) => {
    
    let id = req.params.id
    
    const _id = ObjectId(decodeURI(id))
    
    db.collection('vehicles')
        .findOne({
            _id
        }, function (err, result) {
            if (err) {
                return next(err);
            }
            console.log(result)
            
        res.render('rental.ejs', {
            vehicle: result,
            username: req.params.username
        })
    })
})

app.all('/testing/:username/:id', (req, res, next) => {
    
    let id = req.params.id
    
    const _id = ObjectId(decodeURI(id))
    
    db.collection('vehicles')
        .findOne({
            _id
        }, function (err, result) {
            if (err) {
                return next(err);
            }
            console.log(result)
            
        res.render('testing.ejs', {
            vehicle: result,
            username: req.params.username
        })
    })
})