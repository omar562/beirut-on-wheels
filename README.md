# BeirutOnWheels

This is an implementation of a car dealership website named Beirut On Wheels (BOW) containing different products.

The website is connected to a MongoDB database and uses Express.js (Node.js Framework) to retrieve the Data. On the Front-End, the website is using embeded JavaScript to display the data retrieved.

The website allows a simple signup/login system that increases the priviledges of the user adding the features of renting cars and booking sessions.